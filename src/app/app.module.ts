import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {Formulario1Component} from '../pages/formulario1/formulario1';
import {Formulario2Component} from '../pages/formulario2/formulario2';
import {HeaderComponent} from '../pages/header/header';

import {RegistrarService} from '../services/registrar.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Formulario1Component,
    Formulario2Component,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Formulario1Component,
    Formulario2Component,
    HeaderComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RegistrarService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
