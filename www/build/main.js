webpackJsonp([0],{

/***/ 106:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 106;

/***/ }),

/***/ 147:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 147;

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/home/adonisrq94/capacitacion/src/pages/home/home.html"*/'<app-header></app-header>\n<app-formulario1></app-formulario1>\n'/*ion-inline-end:"/home/adonisrq94/capacitacion/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object])
], HomePage);

var _a;
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Formulario2Component; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_registrar_service__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Formulario2Component = (function () {
    function Formulario2Component(_registrar, navC, NavParams, alertCtrl, formBuilder) {
        this.navC = navC;
        this.NavParams = NavParams;
        this.alertCtrl = alertCtrl;
        this.formBuilder = formBuilder;
        this.formulario = {};
        this.formulario = this.NavParams.get("form");
        this.formulario2 = this.formBuilder.group({
            pais: [''],
            localidad: [''],
            ciudad: [''],
            direccion: [''],
            cod: [''],
            telefono: ['']
        });
    }
    Formulario2Component.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Datos',
            subTitle: 'Nombre:' + this.NavParams.data.form.nombre +
                ' apellido:' + this.NavParams.data.form.apellido +
                ' Genero:' + this.NavParams.data.form.genero +
                ' Fecha:' + this.NavParams.data.form.fecha +
                ' Pais:' + this.formulario2.value.pais +
                ' Localidad:' + this.formulario2.value.localidad +
                ' Ciudad:' + this.formulario2.value.ciudad +
                ' Direccion:' + this.formulario2.value.direccion +
                ' Telefono:' + this.formulario2.value.cod + this.formulario2.value.telefono,
            buttons: ['OK']
        });
        alert.present();
    };
    return Formulario2Component;
}());
Formulario2Component = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-formulario2',template:/*ion-inline-start:"/home/adonisrq94/capacitacion/src/pages/formulario2/formulario2.html"*/'<app-header></app-header>\n<ion-content class="card-formulario">\n<ion-card>\n <form [formGroup]="formulario2" (ngSubmit)="showAlert()" >\n<ion-grid>\n<ion-row>\n\n  <ion-col col-11>\n    <img src="assets/img/logo.jpg" class="logo img-fluid" alt="...">\n  </ion-col>\n  <ion-col class="titulo centrado" col-12><h1>2/2</h1></ion-col>\n  <ion-col class="titulo2 centrado" col-12>Direcciones y Contactos</ion-col>\n\n\n     <ion-col col-6  >\n       <ion-item>\n       <ion-label class=" centrado" stacked>Pais</ion-label>\n       <ion-select  placeholder="Venezuela"class="inputs centrado" formControlName="pais">\n       <ion-option value="Venezuela"  selected="true">Venezuela</ion-option>\n       <ion-option value="Mexico">Mexico</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n\n     <ion-col col-6  >\n       <ion-item>\n       <ion-label class=" centrado" stacked>Edo./Prov./Dpto.</ion-label>\n       <ion-select placeholder="Distrito Capital" class="inputs centrado" formControlName="localidad">\n       <ion-option value="distrito capital" selected="true">Distrito Capital</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n\n\n     <ion-col col-3  >\n       <ion-item  class="centrado">\n       </ion-item>\n     </ion-col >\n\n     <ion-col col-6  >\n       <ion-item>\n       <ion-label class=" centrado" stacked>Ciudad</ion-label>\n       <ion-select  placeholder="Caracas" class="inputs  centrado" formControlName="ciudad">\n       <ion-option value="caracas" selected="true">Caracas</ion-option>\n       <ion-option value="tachira">Tachira</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n\n     <ion-col col-3  >\n       <ion-item  class="centrado">\n       </ion-item>\n     </ion-col >\n\n   <ion-item  class="centrado">\n   <ion-label class=" centrado" stacked>Direccion</ion-label>\n   <ion-input class="inputs" type="text" formControlName="direccion"\n    placeholder="Urb Lorem calle 128 Apt 12 piso 2"></ion-input>\n   </ion-item>\n\n\n     <ion-col col-4>\n       <ion-item>\n       <ion-label class=" centrado" stacked>Cod. de Pais </ion-label>\n       <ion-select placeholder="+58" class="inputs  centrado" formControlName="cod">\n       <ion-option value="+58" selected="true">+58</ion-option>\n     </ion-select>\n       </ion-item>\n     </ion-col >\n     <ion-col col-8>\n       <ion-item  class="centrado">\n       <ion-label class=" centrado" stacked>Telefono</ion-label>\n       <ion-input class="inputs" type="text" formControlName="telefono"\n       placeholder="212 123 45 67"></ion-input>\n       </ion-item>\n     </ion-col >\n\n\n\n\n</ion-row>\n<ion-row>\n  <ion-col col-12 class="boton-siguiente">\n  <button type="submit" class="button-block boton button"ion-button round>Continuar</button>\n  </ion-col>\n</ion-row>\n\n\n</ion-grid>\n</form>\n'/*ion-inline-end:"/home/adonisrq94/capacitacion/src/pages/formulario2/formulario2.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_registrar_service__["a" /* RegistrarService */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], Formulario2Component);

//# sourceMappingURL=formulario2.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegistrarService = (function () {
    function RegistrarService() {
        console.log('servicios inicializado');
    }
    return RegistrarService;
}());
RegistrarService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], RegistrarService);

//# sourceMappingURL=registrar.service.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(213);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_formulario1_formulario1__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_formulario2_formulario2__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_header_header__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_registrar_service__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_formulario1_formulario1__["a" /* Formulario1Component */],
            __WEBPACK_IMPORTED_MODULE_8__pages_formulario2_formulario2__["a" /* Formulario2Component */],
            __WEBPACK_IMPORTED_MODULE_9__pages_header_header__["a" /* HeaderComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */])
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_formulario1_formulario1__["a" /* Formulario1Component */],
            __WEBPACK_IMPORTED_MODULE_8__pages_formulario2_formulario2__["a" /* Formulario2Component */],
            __WEBPACK_IMPORTED_MODULE_9__pages_header_header__["a" /* HeaderComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_10__services_registrar_service__["a" /* RegistrarService */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/adonisrq94/capacitacion/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/adonisrq94/capacitacion/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Formulario1Component; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__formulario2_formulario2__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Formulario1Component = (function () {
    function Formulario1Component(navC, formBuilder) {
        this.navC = navC;
        this.formBuilder = formBuilder;
        this.formulario = this.formBuilder.group({
            nombre: [''],
            apellido: [''],
            genero: [''],
            fecha: ['']
        });
    }
    Formulario1Component.prototype.irFormulario2 = function (form) {
        if (form === void 0) { form = this.formulario.value; }
        console.log(this.formulario.value);
        this.navC.push(__WEBPACK_IMPORTED_MODULE_2__formulario2_formulario2__["a" /* Formulario2Component */], { form: form });
    };
    return Formulario1Component;
}());
Formulario1Component = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-formulario1',template:/*ion-inline-start:"/home/adonisrq94/capacitacion/src/pages/formulario1/formulario1.html"*/'\n\n<ion-content class="card-formulario">\n<ion-card>\n <form  [formGroup]="formulario" (ngSubmit)="irFormulario2()">\n<ion-grid>\n<ion-row>\n\n  <ion-col col-11>\n    <img src="assets/img/logo.jpg" class="logo img-fluid" alt="...">\n  </ion-col>\n  <ion-col class="titulo centrado" col-12><h1>1/2</h1></ion-col>\n  <ion-col class="titulo2 centrado" col-12>Datos Personales</ion-col>\n   <ion-col col-12>\n  <ion-list>\n  <ion-item >\n  <ion-label class=" centrado" stacked>Nombre</ion-label>\n  <ion-input class="inputs" type="text" placeholder="Maria" formControlName="nombre"></ion-input>\n  </ion-item>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Apellido</ion-label>\n  <ion-input class="inputs" type="text" placeholder="Lopez" formControlName="apellido"></ion-input>\n  </ion-item>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Genero</ion-label>\n  <ion-select   placeholder="Masculino"\n  class="inputs  centrado" formControlName="genero">\n  <ion-option  value="Masculino" selected="true" >Masculino</ion-option>\n  <ion-option value="Femenino">Femenino</ion-option>\n</ion-select>\n  </ion-item>\n  <ion-item>\n  <ion-label class=" centrado" stacked>Fecha nacimiento</ion-label>\n  <ion-datetime  class="inputs " formControlName="fecha"\n  displayFormat="MM  DD  YYYY" pickerFormat="MM  DD YYYY" placeholder="01 01 2017" ></ion-datetime>\n  </ion-item>\n</ion-list>\n  </ion-col>\n</ion-row>\n<ion-row>\n  <ion-col col-12 class="boton-siguiente">\n  <button type="submit" block class="button-block boton button"\n  ion-button round>Siguiente</button>\n  </ion-col>\n</ion-row>\n\n\n</ion-grid>\n</form>\n\n\n\n\n\n\n</ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/adonisrq94/capacitacion/src/pages/formulario1/formulario1.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], Formulario1Component);

//# sourceMappingURL=formulario1.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var HeaderComponent = (function () {
    function HeaderComponent() {
        console.log('Hello HeaderComponent Component');
        this.text = 'Hello World';
    }
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-header',template:/*ion-inline-start:"/home/adonisrq94/capacitacion/src/pages/header/header.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n        <ion-icon class="icono-more" ios="ios-more" md="md-more"></ion-icon>\n        <ion-icon class="icono" ios="ios-bookmark" md="md-bookmark"></ion-icon>\n        <ion-icon class="icono" ios="ios-share" md="md-share"></ion-icon>\n\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n'/*ion-inline-end:"/home/adonisrq94/capacitacion/src/pages/header/header.html"*/
    }),
    __metadata("design:paramtypes", [])
], HeaderComponent);

//# sourceMappingURL=header.js.map

/***/ })

},[194]);
//# sourceMappingURL=main.js.map